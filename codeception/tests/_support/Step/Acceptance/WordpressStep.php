<?php
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\LoginPage;
use \Page\Acceptance\Admin\SidebarPage;

namespace Step\Acceptance;

class WordpressStep extends \AcceptanceTester
{

    public function logout()
    {
        $I = $this;

        $header = new \Page\Acceptance\AdminHeaderPage($I);
        $header->clickLogout();
        
        $I->waitForText('You are now logged out.');
    }

    public function login($user = 'wp_admin', $pass = 'Testing123')
    {
        $I = $this;
        $I->amOnPage(\Page\Acceptance\LoginPage::$URL);
        
        $login = new \Page\Acceptance\LoginPage($I);
        $login->loginToWordpress($user, $pass);

        $I->waitForElementVisible(\Page\Acceptance\AdminHeaderPage::$headerDiv);
    }

    public function navigateAdminSidebar($topLevel, $subMenu)
    {
        $I = $this;

        $sidebar = new \Page\Acceptance\Admin\SidebarPage($I);
        $sidebar->navigate($topLevel, $subMenu);
    }
}