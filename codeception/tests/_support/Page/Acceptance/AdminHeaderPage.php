<?php
namespace Page\Acceptance;

class AdminHeaderPage
{
    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    public static $headerDiv = '//div [@id="wpadminbar"]';
    public static $siteNameLink = '//* [@id="wp-admin-bar-site-name"]//a';
    public static $viewPage = '//* [@id="wp-admin-bar-view"]//a';
    public static $new = '//* [@id="wp-admin-bar-new-content"]//a';

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

    public static function getSiteTitleLink()
    {
        return SELF::$headerDiv.'//* [@id="wp-admin-bar-site-name"]//a';
    }

    public static function getHowdyLink()
    {
        return SELF::$headerDiv.'//* [@id="wp-admin-bar-my-account"]//a';
    }

    public static function getLogoutLink()
    {
        return SELF::$headerDiv.'//* [@id="wp-admin-bar-logout"]//a';
    }

    public function clickLogout()
    {
        $I = $this->acceptanceTester;
        $I->moveMouseOver(SELF::getHowdyLink());
        $I->waitForElementVisible(SELF::getLogoutLink());
        $I->click(SELF::getLogoutLink());
    }
}
