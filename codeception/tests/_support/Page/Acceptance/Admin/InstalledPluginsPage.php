<?php
namespace Page\Acceptance\Admin;

class InstalledPluginsPage
{
    // include url of current page
    public static $URL = 'wp-admin/plugins.php';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */


    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    protected static function getPluginRowByName($pluginName)
    {
        return '//* [@data-slug="' . $pluginName . '"]';
    }

    public static function getActivateLinkByPluginName($pluginName)
    {
        return SELF::getPluginRowByName($pluginName) . '//* [@class="activate"]//a';
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

}
