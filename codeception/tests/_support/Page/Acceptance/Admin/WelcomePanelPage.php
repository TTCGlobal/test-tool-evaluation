<?php
namespace Page\Acceptance\Admin;

class WelcomePanelPage
{
    // include url of current page
    public static $URL = 'wp-admin/';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */
    public static $addAboutPage = '//* [@class="welcome-icon welcome-add-page"]';
    public static $addNewBlog = '//a [@class="welcome-icon welcome-write-blog"]';
    public static $disableComments = '//a [@class="welcome-icon welcome-comments"]';

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

}
