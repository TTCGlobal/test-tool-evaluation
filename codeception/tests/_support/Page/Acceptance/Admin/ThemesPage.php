<?php
namespace Page\Acceptance\Admin;

class ThemesPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */
    public static $themeDiv = '//* [contains(@class, "theme")]';

    public static $modalActivate = '//a[text()="Activate"]';
    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static function getLastThemeDiv()
    {
        return SELF::$themeDiv . '[last()]';
    }

    public static function getActivationLinkFromDiv($div)
    {
        return $div . '//a[text()="Activate"]';
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }
}
