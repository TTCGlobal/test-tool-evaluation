<?php
namespace Page\Acceptance\Admin;

class AddNewPostPage
{
    // include url of current page
    public static $URL = 'wp-admin/post-new.php';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */
    public static $title = '//* [@id="title"]';
    public static $addMedia = '//* [@id="insert-media-button"]';
    public static $visual = '//* [@id="content-tmce"]';
    public static $text = '//* [@id="content-html"]';

    // TinyMCE Visual Editor
    public static $tmceBold = '//*[@aria-label="Bold"]';

    public static $tmceIframe = '//* [@id="content_ifr"]';
    public static $tmceBody = '//* [@id="tinymce"]';

    // Plain HTML Editor
    public static $textContent = '//* [@id="content"]';

    // Publish Sidebar Panel
    public static $publishButton = '//* [@id="publish"]';
    public static $status = '//* [@id="post-status-display"]';

    // Add New Media Modal
    public static $fromMediaLibrary = '//* [@class="media-modal-content"]//a[text()="Media Library"]';
    public static $insertIntoPost = '//button [text()="Insert into post"]';
    public static $closeAddMediaModal = '//* [@class="button-link media-modal-close"]';
    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

}
