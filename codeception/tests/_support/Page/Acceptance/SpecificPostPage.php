<?php
namespace Page\Acceptance;

class SpecificPostPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */
    public static $contentTitle = '//* [@class="entry-title"]';
    public static $contentBody = '//* [@class="entry-content"]';

    // comments
    public static $commentsDiv = '//* [@id="comments"]';
    
    // post comment
    public static $respondDiv = '//* [@id="respond"]';
    public static $newComment = '//* [@id="comment"]';
    public static $postComment = '//* [@id="submit"]';

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static function getImageInContentByName($name)
    {
        return SELF::$contentBody . '//img [contains(@src, "' . $name . '")]';
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

}
