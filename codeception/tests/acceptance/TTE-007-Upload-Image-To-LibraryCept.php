<?php 
use \Page\Acceptance\Admin\MediaLibraryNewPage;
use \Page\Acceptance\Admin\MediaLibraryPage;
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\Admin\AddNewPostPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Upload an Image to the Media Library');

$I->login();

$I->navigateAdminSidebar('Media', 'Add New');

// If we are on flash uploader page, switch to non-flash
if ($I->grabMultiple(MediaLibraryNewPage::$flashToUploadLink)[0] != "") {
    $I->click(MediaLibraryNewPage::$flashToUploadLink);
}

$I->attachFile(MediaLibraryNewPage::$fileName, 'image.png');
$I->click(MediaLibraryNewPage::$uploadButton);

$I->seeElement(MediaLibraryPage::getSpecificImageThumbnailByName('image'));

// check its loaded from a new post page
$I->click(AdminHeaderPage::$new);

$I->click(AddNewPostPage::$addMedia);

$I->click(AddNewPostPage::$fromMediaLibrary);
$I->seeElement(MediaLibraryPage::getSpecificImageThumbnailByName('image'));

$I->click(AddNewPostPage::$closeAddMediaModal);

$I->logout();