<?php 
use \Page\Acceptance\Admin\WelcomePanelPage;
use \Page\Acceptance\Admin\AddNewPostPage;
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\SpecificPostPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Add An About Page w/Rich HTML Editor');

$I->login();

$I->click(WelcomePanelPage::$addAboutPage);

$I->fillField(AddNewPostPage::$title, 'Testing Tool Evaluation Project SUT');
$I->click(AddNewPostPage::$visual);

// Add Body Text
$I->switchToIFrame(AddNewPostPage::$tmceIframe);
$I->pressKey(AddNewPostPage::$tmceBody, "This is regular text.");
$I->pressKey(AddNewPostPage::$tmceBody, \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->pressKey(AddNewPostPage::$tmceBody, array('ctrl', 'b'));

$I->pressKey(AddNewPostPage::$tmceBody, "This is bold text.");
$I->pressKey(AddNewPostPage::$tmceBody, \Facebook\WebDriver\WebDriverKeys::ENTER);
$I->pressKey(AddNewPostPage::$tmceBody, 
"The TTE project aims to provide a baseline of test plans for comparison of test tools and how much work is required to maintain the tests over time."
);
$I->switchToIFrame(); //switch back to parent

$I->click(AddNewPostPage::$publishButton);
$I->waitForText('Published');
$I->see('Published');

$I->click(AdminHeaderPage::$viewPage);
$I->see('Testing Tool Evaluation Project SUT', SpecificPostPage::$contentTitle);
$I->see('This is bold text.', SpecificPostPage::$contentBody.'//strong');

$I->logout();