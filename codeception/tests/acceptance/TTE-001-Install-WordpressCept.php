<?php 

use \Page\Acceptance\Installer\Step1Page;
use \Page\Acceptance\Installer\Step2Page;
use \Page\Acceptance\Installer\Step3Page;
use \Page\Acceptance\LoginPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Install Wordpress via Web Installer in TTE Docker Env');

// Step 1
$I->selectOption(Step1Page::$language, 'English (United States)');
$I->click(Step1Page::$continue);

// Step 2
$I->fillField(Step2Page::$title, 'TTC Test Site');
$I->fillField(Step2Page::$user, 'wp_admin');
$I->clearField(Step2Page::$password);
$I->pressKey(Step2Page::$password, 'Testing123');
$I->seeElement(Step2Page::$confirmWeakPassword);
$I->checkOption(Step2Page::$confirmWeakPassword);
$I->fillField(Step2Page::$email, 'noreply@localhost.local');
$I->click(Step2Page::$installWordpress);

// Confirm Success
$I->see('Success!');
$I->see('wp_admin');
$I->click(Step3Page::$login);

// Login Page
$I->seeInCurrentUrl(LoginPage::$URL);
$login = new LoginPage($I);
$login->loginToWordpress();

// Confirm Login Worked
$I->see("Welcome to WordPress!");
$I->see("We’ve assembled some links to get you started:");

// Logout Via Header Bar
$I->logout();