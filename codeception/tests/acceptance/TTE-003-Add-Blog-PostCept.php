<?php 
use \Page\Acceptance\Admin\WelcomePanelPage;
use \Page\Acceptance\Admin\AddNewPostPage;
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\SpecificPostPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Add A Blog Entry with Standard Editor');

$I->login();

$I->click(WelcomePanelPage::$addNewBlog);

$I->fillField(AddNewPostPage::$title, 'First Blog Post');
$I->click(AddNewPostPage::$text);

$I->fillField(AddNewPostPage::$textContent, 
'<strong>This is a bold line.</strong>
<br>
This project is done as part of <a href="https://ttcglobal.com">TTC\'s</a> on going research to ensure we can provided the best advice to our clients.  
');

$I->click(AddNewPostPage::$publishButton);
$I->waitForText('Published');
$I->see('Published');

$I->click(AdminHeaderPage::$viewPage);
$I->see('First Blog Post', SpecificPostPage::$contentTitle);
$I->see('This is a bold line.', SpecificPostPage::$contentBody.'//strong');

$I->logout();