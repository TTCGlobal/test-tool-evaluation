# Test Cases

## Overview
These test cases are intended to represent some basic functional tests for a Wordpress installation as the SUT. Design goals for the tests are:

1. To represent a neutral set of requirements that every mainstream web based automation testing tool can automate.
2. To require some small / advanced test case work to stress the flexibility of the SUT under more dynamic situations. 
3. To provide a useful baseline for measuring maintenance over time. 

## Testcases

### TTE-1 Can compleate Wordpress Installation (enter details, can use to login, can verify that expected version is installed)

#### Precondition
1. Setup TTE Docker environment locally on your machine. 
2. Ensure DB wp_db is dropped.
3. Start SUT containers using docker-compose. 
#### Process
1. Open Browser and visit http://localhost:8080/
2. Assert you are redirected to WP Install Page. 
3. Select English as Language.
4. Click continue.  
5. Enter 'TTC Test Site' in Site Title box.
6. Enter 'wp_admin' in Username box.
7. Enter 'Testing123!' in Password box.
8. Check Confirm Use of Weak Password.
9. Enter 'noreply@localhost.local' in email box.
10. Click Continue button.
11. Assert you are taken to WP Install Success page.
12. Assert the correct username is shown on screen.
13. Assert your password is not shown on screen.
14. Click Login
15. Assert you are taken to Login Screen.
16. Login with username 'wp_admin' & password 'Testing123!'
17. Assert you are taken to Wordpress Admin Dashboard. 
#### Postcondition
1. Log out.
2. Close Browser. 

### TTE-2 Add About Page

#### Precondition
1. Successfully compleated TTE-1.
#### Process
1. Open Browser to http://localhost:8080/wp-login.php
2. Login with username 'wp_admin' & password 'Testing123!'
3. In the getting started section of the dashboard, click the link: "Add an About page"
4. Enter "Testing Tool Evaluation Project SUT" in the Title box. 
5. In the body area use the visual editor to add the following text, using the editor toolbar to add bold for the required line:

This is regular text.

**This is bold text.**

The TTE project aims to provide a baseline of test plans for comparison of test tools and how much work is required to maintain the tests over time. 

6. Click the Publish button.
7. Assert Status is updated to Published. 
8. Click View Page from top toolbar. 
9. Assert that Title matched input. 
10. Assert that bold sentance is bold. 
#### Postcondition
1. Log out.
2. Close Browser. 

### TTE-3 Add Your First Blog Post

#### Precondition
1. Successfully compleated TTE-1.
#### Process
1. Open Browser to http://localhost:8080/wp-login.php
2. Login with username 'wp_admin' & password 'Testing123!'
3. In the getting started section of the dashboard, click the link: "Write your first blog post"
4. Enter "First Blog Post" into the Title box. 
5. Switch to the Text and input html tagged content blog:
`
<strong>This is a bold line.</strong>
<br>
This project is done as part of <a href="https://ttcglobal.com">TTC's</a> on going research to ensure we can provided the best advice to our clients.  
`
6. Click the Publish button.
7. Assert Status is updated to Published. 
8. Click View Post from top toolbar.
9. Assert line is bold.
10. Assert link is active. 
11. Click title "TTC Test Site".
12. Verify "First Blog Post" is shown on homepage. 
#### Postcondition
1. Log out.
2. Close Browser. 

### TTE-4 Change Site Title & Confirm it changes on home page

#### Precondition
1. Successfully compleated TTE-1.
#### Process
1. Open Browser to http://localhost:8080/wp-login.php
2. Login with username 'wp_admin' & password 'Testing123!'
3. Navigate to Settings -> General in sidebar menu. 
4. Change Site Title box to: "TTE Site Title"
5. Click Save Changes button.
6. Assert Site Title Bar changes to new text. 
7. Click Site Title to go to home page. 
8. Assert Home Page displays new title. 
#### Postcondition
1. Log out.
2. Close Browser.

### TTE-5 Confirm You Can Comment on First Post

#### Precondition
1. Successfully compleated TTE-1.
#### Process
1. Open Browser to http://localhost:8080/wp-login.php
2. Login with username 'wp_admin' & password 'Testing123!'
3. Click on home page link. 
4. Click on "Hello world!" blog title. 
5. Enter "foobar this is a comment" + datetime in the comment box. 
6. Click post comment button. 
7. Assert comment text is present.
8. Assert comment has today's date. 
#### Postcondition
1. Log out.
2. Close Browser.

### TTE-6 Disable Commenting on New Articles and Confirm You Cannot Comment

#### Precondition
1. Successfully compleated TTE-1.
#### Process
1. Open Browser to http://localhost:8080/wp-login.php
2. Login with username 'wp_admin' & password 'Testing123!'
3. In the Welcome to WordPress box, click the 'Turn comments on or off' link.
4. In the Discussion Settings Page, uncheck "Allow people to post comments on new articles"
5. Ensure "Users ust be registered and logged in to comment" is unchecked.
6. Uncheck "Comment author must have a previously approved comment" checkbox. 
7. Click Save Changes button.
8. Click New Post Link in top toolbar.
9. Enter title: "Can't comment here"
10. Enter body: "Don't comment on me."
11. Click Publish.
12. Click View Post. 
13. Confirm no comment box appears. 
#### Postcondition
1. Log out.
2. Close Browser.

### TTE-7 Upload an image into the media library

#### Precondition
1. Successfully compleated TTE-1.
#### Process
1. Open Browser to http://localhost:8080/wp-login.php
2. Login with username 'wp_admin' & password 'Testing123!'
3. Click on the Media -> New link via the sidebar. 
4. Click on use browser uploader.
5. Add fle via upload button. 
6. Click upload.
7. Assert you see the file name in the list of uploaded media. 
8. Click new post from top bar. 
9. Click add media button.
10. Click Media Library Tab.
11. Verify image is present in media library tab. 

#### Postcondition
1. Log out.
2. Close Browser.

### TTE-8 Edit a story and insert a image from the media library

#### Precondition
1. Successfully compleated TTE-1.
2. Successfully compleated TTE-7. 
#### Process
1. Open Browser to http://localhost:8080/wp-login.php
2. Login with username 'wp_admin' & password 'Testing123!'
3. Click new post from top bar. 
4. Click add media button.
5. Click Media Library Tab.
6. Select Image from Media Library.  
7. Assert img tag is present in text tab. 
8. Click Publish button. 
9. Click View Post link in header. 
10. Verify img present on screen. 

#### Postcondition
1. Log out.
2. Close Browser.

### TTE-9 Activate Hello Dolly Plugin and Confirm it Works

#### Precondition
1. Successfully compleated TTE-1.
#### Process
1. Open Browser to http://localhost:8080/wp-login.php
2. Login with username 'wp_admin' & password 'Testing123!'
3. Click on Plugins -> Installed Plugins 
4. Assert you see Hello Dolly plugin. 
5. Click Activate link for Hello Dolly plugin. 
6. Assert it shows as activated. 
7. Click back to dashboard homepage. Confirm new text is present near screen options dropdown. 
8. Confirm text is present in lyrics to the song Hello, Dolly. 

#### Postcondition
1. Log out.
2. Close Browser.

### TTE-10 Switch to the oldest theme installed and confirm the home page screen looks different

#### Precondition
1. Successfully compleated TTE-1.

#### Process
1. Open Browser - Note current theme in use.
2. Navigate to http://localhost:8080/wp-login.php
3. Login with username 'wp_admin' & password 'Testing123!'
4. Click on Appearance -> Themes
5. Hover over theme icon. 
6. Click activate button.
7. Assert "New Theme Activated" status shown. 
8. Click "Visit site" link in status. 
9. Assert new theme is in use. 

#### Postcondition
1. Log out.
2. Close Browser.
