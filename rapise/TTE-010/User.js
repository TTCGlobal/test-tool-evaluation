//Put your custom functions and variables in this file

function GetCssLinks()
{
    var cssLinks = [];
    var head = Navigator.Find("//head");
    var cc = head._DoDOMChildrenCount();
    for(var i = 0; i < cc; i++)
    {
        var c = head._DoDOMChildAt(i);
        var tag = c.GetTag();
        if (tag.toLowerCase() == "link")
        {
            var rel = c._DoDOMGetAttribute("rel");
            if (rel && rel.toLowerCase() == "stylesheet")
            {
                var href = c._DoDOMGetAttribute("href");
                if (href)
                {
                    cssLinks.push(href);
                }
            }
        }
    }
    return cssLinks;
}