var saved_script_objects={
	"Username_or_Email": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Username or Email\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='log' and @id='user_login']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "text"
						},
						{
							"name": "name",
							"value": "log"
						},
						{
							"name": "id",
							"value": "user_login"
						},
						{
							"name": "class",
							"value": "input"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "size",
							"value": "20"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "user_login"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\5e20d9ea-c6b5-fd70-bd0b-5ca7c267be9a.png"
	},
	"wp_submit": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "wp-submit",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='wp-submit' and @id='wp-submit']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "name",
							"value": "wp-submit"
						},
						{
							"name": "id",
							"value": "wp-submit"
						},
						{
							"name": "class",
							"value": "button button-primary button-large"
						},
						{
							"name": "value",
							"value": "Log In"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "submit"
						}
					],
					"indexInParent": 3,
					"level": 1
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 5
				}
			]
		},
		"screenshot": "Recording\\434207e0-95bf-7867-6ddf-02cf38a79262.png"
	},
	"Password": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Password\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='pwd' and @id='user_pass']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "password"
						},
						{
							"name": "name",
							"value": "pwd"
						},
						{
							"name": "id",
							"value": "user_pass"
						},
						{
							"name": "class",
							"value": "input"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "size",
							"value": "20"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "user_pass"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\5048d43d-acc3-cd6b-4175-869eadd2b702.png"
	},
	"Turn_comments_on_or_off": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Turn comments on or off",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//div[3]/ul/li[2]/a",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/options-discussion.php"
						},
						{
							"name": "class",
							"value": "welcome-icon welcome-comments"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Turn comments on or off"
				},
				{
					"tagName": "li",
					"attributes": [],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "welcome-panel-column welcome-panel-last"
						}
					],
					"indexInParent": 2,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "welcome-panel-column-container"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "welcome-panel-content"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "welcome-panel"
						},
						{
							"name": "class",
							"value": "welcome-panel"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 13
				}
			]
		},
		"screenshot": "Recording\\f99636ec-5835-40c7-1f23-072154cdd227.png"
	},
	"Allow_people_to_post_comments_on": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "CheckBox",
		"object_name": "\n\nAllow people to post comments on new articles",
		"object_library": "Chrome HTML",
		"window_name": "Discussion Settings ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='default_comment_status' and @id='default_comment_status']",
		"title": "Discussion Settings ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/options-discussion.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "name",
							"value": "default_comment_status"
						},
						{
							"name": "type",
							"value": "checkbox"
						},
						{
							"name": "id",
							"value": "default_comment_status"
						},
						{
							"name": "value",
							"value": "open"
						},
						{
							"name": "checked",
							"value": "checked"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "default_comment_status"
						}
					],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "fieldset",
					"attributes": [],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "td",
					"attributes": [],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "tr",
					"attributes": [],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "tbody",
					"attributes": [],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "table",
					"attributes": [
						{
							"name": "class",
							"value": "form-table"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "action",
							"value": "options.php"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 14
				}
			]
		},
		"screenshot": "Recording\\f15b7aab-2b1d-9d88-c48c-8ccd0881b877.png"
	},
	"Comment_author_must_have_a_previ": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "CheckBox",
		"object_name": " Comment author must have a previously approved comment",
		"object_library": "Chrome HTML",
		"window_name": "Discussion Settings ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='comment_whitelist' and @id='comment_whitelist']",
		"title": "Discussion Settings ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/options-discussion.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "checkbox"
						},
						{
							"name": "name",
							"value": "comment_whitelist"
						},
						{
							"name": "id",
							"value": "comment_whitelist"
						},
						{
							"name": "value",
							"value": "1"
						},
						{
							"name": "checked",
							"value": "checked"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "comment_whitelist"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "fieldset",
					"attributes": [],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "td",
					"attributes": [],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "tr",
					"attributes": [],
					"indexInParent": 3,
					"level": 4
				},
				{
					"tagName": "tbody",
					"attributes": [],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "table",
					"attributes": [
						{
							"name": "class",
							"value": "form-table"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "action",
							"value": "options.php"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 14
				}
			]
		},
		"screenshot": "Recording\\4ddc7042-4cb1-967f-e5ea-d8cd167c0606.png"
	},
	"submit": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "submit",
		"object_library": "Chrome HTML",
		"window_name": "Discussion Settings ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='submit' and @id='submit']",
		"title": "Discussion Settings ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/options-discussion.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "name",
							"value": "submit"
						},
						{
							"name": "id",
							"value": "submit"
						},
						{
							"name": "class",
							"value": "button button-primary"
						},
						{
							"name": "value",
							"value": "Save Changes"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "submit"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "action",
							"value": "options.php"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Recording\\695112c7-6a4a-ca6b-ec92-a27a90c80e8a.png"
	},
	"_New": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "New",
		"object_library": "Chrome HTML",
		"window_name": "Discussion Settings ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-new-content']/a",
		"title": "Discussion Settings ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/options-discussion.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/post-new.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "New"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-content"
						},
						{
							"name": "class",
							"value": "menupop hover"
						}
					],
					"indexInParent": 5,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-root-default"
						},
						{
							"name": "class",
							"value": "ab-top-menu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Recording\\a381a32b-90c8-5b64-df55-b2536ccf4116.png"
	},
	"Post": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Post",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-new-post']/a",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/post-new.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Post"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-post"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-content-default"
						},
						{
							"name": "class",
							"value": "ab-submenu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "ab-sub-wrapper"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-content"
						},
						{
							"name": "class",
							"value": "menupop hover"
						}
					],
					"indexInParent": 5,
					"level": 4
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-root-default"
						},
						{
							"name": "class",
							"value": "ab-top-menu"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\9da6bd86-650e-9fb1-9f00-2f87bca6218f.png"
	},
	"Enter_title_here": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Enter title here",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='post_title' and @id='title']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "text"
						},
						{
							"name": "name",
							"value": "post_title"
						},
						{
							"name": "size",
							"value": "30"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "id",
							"value": "title"
						},
						{
							"name": "spellcheck",
							"value": "true"
						},
						{
							"name": "autocomplete",
							"value": "off"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "titlewrap"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "titlediv"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 13
				}
			]
		},
		"screenshot": "Recording\\6dee5526-d71f-58e9-3993-76fe61679acf.png"
	},
	"wpbody_content": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "wpbody-content",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//div[@id='wpbody-content']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 5
				}
			]
		},
		"screenshot": "Recording\\e94b1116-9fcf-21c8-58db-c606e8d13ab6.png"
	},
	"Text": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "Text",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//button[@id='content-html']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "button",
					"attributes": [
						{
							"name": "type",
							"value": "button"
						},
						{
							"name": "id",
							"value": "content-html"
						},
						{
							"name": "class",
							"value": "wp-switch-editor switch-html"
						},
						{
							"name": "data-wp-editor-id",
							"value": "content"
						}
					],
					"indexInParent": 1,
					"level": 0,
					"text": "Text"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wp-editor-tabs"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-editor-tools"
						},
						{
							"name": "class",
							"value": "wp-editor-tools hide-if-no-js"
						},
						{
							"name": "style",
							"value": "position: absolute; top: 0px; width: 1401px;"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-wrap"
						},
						{
							"name": "class",
							"value": "wp-core-ui wp-editor-wrap html-active has-dfw"
						},
						{
							"name": "style",
							"value": "padding-top: 55px;"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postdivrich"
						},
						{
							"name": "class",
							"value": "postarea wp-editor-expand"
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 12
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 14
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 15
				}
			]
		},
		"screenshot": "Recording\\3f252730-ca96-3ec8-db7b-300b578ee58e.png"
	},
	"content": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "content",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//textarea[@name='content' and @id='content']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "textarea",
					"attributes": [
						{
							"name": "class",
							"value": "wp-editor-area"
						},
						{
							"name": "style",
							"value": "height: 320px; margin-top: 37px;"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "cols",
							"value": "40"
						},
						{
							"name": "name",
							"value": "content"
						},
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "aria-hidden",
							"value": "false"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-editor-container"
						},
						{
							"name": "class",
							"value": "wp-editor-container"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-wrap"
						},
						{
							"name": "class",
							"value": "wp-core-ui wp-editor-wrap html-active has-dfw"
						},
						{
							"name": "style",
							"value": "padding-top: 55px;"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postdivrich"
						},
						{
							"name": "class",
							"value": "postarea wp-editor-expand"
						}
					],
					"indexInParent": 1,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 14
				}
			]
		},
		"screenshot": "Recording\\27f15eb7-9c89-0a85-f954-a847512159a6.png"
	},
	"publish": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "publish",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='publish' and @id='publish']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "name",
							"value": "publish"
						},
						{
							"name": "id",
							"value": "publish"
						},
						{
							"name": "class",
							"value": "button button-primary button-large"
						},
						{
							"name": "value",
							"value": "Publish"
						}
					],
					"indexInParent": 1,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "publishing-action"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "major-publishing-actions"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "submitbox"
						},
						{
							"name": "id",
							"value": "submitpost"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "inside"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "submitdiv"
						},
						{
							"name": "class",
							"value": "postbox "
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "side-sortables"
						},
						{
							"name": "class",
							"value": "meta-box-sortables ui-sortable"
						},
						{
							"name": "style",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postbox-container-1"
						},
						{
							"name": "class",
							"value": "postbox-container"
						}
					],
					"indexInParent": 1,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 13
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 14
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 15
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 16
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 17
				}
			]
		},
		"screenshot": "Recording\\27ab236d-6975-76d7-9453-28864e6e1c3a.png"
	},
	"wpadminbar": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "wpadminbar",
		"object_library": "Chrome HTML",
		"window_name": "Edit Post ‹ TTE Site Title — WordPress",
		"xpath": "//div[@id='wpadminbar']",
		"title": "Edit Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post.php?post=24&action=edit",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 4
				}
			]
		},
		"screenshot": "Recording\\a537d230-2820-5bc3-96a0-3ddfd3f2a898.png"
	},
	"View_Post": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "View Post",
		"object_library": "Chrome HTML",
		"window_name": "Edit Post ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-view']/a",
		"title": "Edit Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post.php?post=24&action=edit",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/?p=24"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "View Post"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-view"
						}
					],
					"indexInParent": 6,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-root-default"
						},
						{
							"name": "class",
							"value": "ab-top-menu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Recording\\d7a4a820-d6ac-8d1a-7f36-847dd892fa92.png"
	},
	"Howdy__wp_admin": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Howdy, wp_admin",
		"object_library": "Chrome HTML",
		"window_name": "Can’t Comment Here – TTE Site Title",
		"xpath": "//li[@id='wp-admin-bar-my-account']/a",
		"title": "Can’t Comment Here – TTE Site Title",
		"url": "http://localhost:8080/?p=24",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/profile.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Howdy, wp_admin"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-my-account"
						},
						{
							"name": "class",
							"value": "menupop with-avatar hover"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-top-secondary"
						},
						{
							"name": "class",
							"value": "ab-top-secondary ab-top-menu"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "single single-post postid-24 single-format-standard logged-in admin-bar  customize-support"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\ca51fd3e-0021-eff2-7dd7-db62ce1fcd28.png"
	},
	"Log_Out": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Log Out",
		"object_library": "Chrome HTML",
		"window_name": "Profile ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-logout']/a",
		"title": "Profile ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/profile.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-login.php?action=logout&_wpnonce=2c6a0329ba"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Log Out"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-logout"
						}
					],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-user-actions"
						},
						{
							"name": "class",
							"value": "ab-submenu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "ab-sub-wrapper"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-my-account"
						},
						{
							"name": "class",
							"value": "menupop with-avatar hover"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-top-secondary"
						},
						{
							"name": "class",
							"value": "ab-top-secondary ab-top-menu"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\f8fcb39c-f416-08d3-bf71-5934fc692681.png"
	},
	"login": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "login",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//div[@id='login']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 2
				}
			]
		},
		"screenshot": "Recording\\44aa0405-8186-bbae-cbd8-4b0d20090a24.png"
	},
	"You_are_now_logged_out_": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "\tYou are now logged out.\n",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "You are now logged out.",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//div[@id='login']/p[1]",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "message"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				}
			]
		},
		"screenshot": "Recording\\42831134-45bd-3697-15ec-c6507dc56a72.png"
	},
	"Comment": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Comment",
		"object_library": "Chrome HTML",
		"window_name": "First Blog Post – TTE Site Title",
		"xpath": "//textarea[@name='comment' and @id='comment']",
		"title": "First Blog Post – TTE Site Title",
		"url": "http://localhost:8080/?p=21",
		"fullpath": {
			"path": [
				{
					"tagName": "textarea",
					"attributes": [
						{
							"name": "id",
							"value": "comment"
						},
						{
							"name": "name",
							"value": "comment"
						},
						{
							"name": "cols",
							"value": "45"
						},
						{
							"name": "rows",
							"value": "8"
						},
						{
							"name": "maxlength",
							"value": "65525"
						},
						{
							"name": "aria-required",
							"value": "true"
						},
						{
							"name": "required",
							"value": "required"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "comment-form-comment"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "action",
							"value": "http://localhost:8080/wp-comments-post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "commentform"
						},
						{
							"name": "class",
							"value": "comment-form"
						},
						{
							"name": "novalidate",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "respond"
						},
						{
							"name": "class",
							"value": "comment-respond"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "comments"
						},
						{
							"name": "class",
							"value": "comments-area"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "main",
					"attributes": [
						{
							"name": "id",
							"value": "main"
						},
						{
							"name": "class",
							"value": "site-main"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "primary"
						},
						{
							"name": "class",
							"value": "content-area"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "class",
							"value": "site-content"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "site-inner"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "page"
						},
						{
							"name": "class",
							"value": "site"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "single single-post postid-21 single-format-standard"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\8e1ce1d3-43cd-f065-686d-8477c9eabce3.png"
	}
};