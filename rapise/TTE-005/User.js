//Put your custom functions and variables in this file

function generateCommentText()
{
	return "foobar this is a comment " + Math.round((new Date()).getTime() / 1000);
}

function getDateForVerification()
{
	var d = new Date();
	return d.getDate() + ", " + d.getFullYear();
}