var saved_script_objects = {
	"Comment": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Comment",
		"object_library": "Chrome HTML",
		"window_name": "Hello world! – TTE Site Title",
		"xpath": "//textarea[@name='comment' and @id='comment']",
		"title": "Hello world! – TTE Site Title",
		"url": "http://localhost:8080/?p=1",
		"fullpath": {
			"path": [
				{
					"tagName": "textarea",
					"attributes": [
						{
							"name": "id",
							"value": "comment"
						},
						{
							"name": "name",
							"value": "comment"
						},
						{
							"name": "cols",
							"value": "45"
						},
						{
							"name": "rows",
							"value": "8"
						},
						{
							"name": "maxlength",
							"value": "65525"
						},
						{
							"name": "aria-required",
							"value": "true"
						},
						{
							"name": "required",
							"value": "required"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "comment-form-comment"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "action",
							"value": "http://localhost:8080/wp-comments-post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "commentform"
						},
						{
							"name": "class",
							"value": "comment-form"
						},
						{
							"name": "novalidate",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "respond"
						},
						{
							"name": "class",
							"value": "comment-respond"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "comments"
						},
						{
							"name": "class",
							"value": "comments-area"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "main",
					"attributes": [
						{
							"name": "id",
							"value": "main"
						},
						{
							"name": "class",
							"value": "site-main"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "primary"
						},
						{
							"name": "class",
							"value": "content-area"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "class",
							"value": "site-content"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "site-inner"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "page"
						},
						{
							"name": "class",
							"value": "site"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "single single-post postid-1 single-format-standard logged-in admin-bar  customize-support"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Reports\\0d1c7c57-c073-369a-e0cc-2bceb4faca90.png"
	}
}