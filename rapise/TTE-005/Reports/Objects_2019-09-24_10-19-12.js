var saved_script_objects = {
	"TTE_Site_Title": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "TTE Site Title",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-site-name']/a",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "TTE Site Title"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-site-name"
						},
						{
							"name": "class",
							"value": "menupop"
						}
					],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-root-default"
						},
						{
							"name": "class",
							"value": "ab-top-menu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Reports\\e9de3b34-414f-1745-1137-f46a5b54e72c.png"
	}
}