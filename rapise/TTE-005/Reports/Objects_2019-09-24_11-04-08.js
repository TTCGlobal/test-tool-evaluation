var saved_script_objects = {
	"foobar_this_is_a_comment": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "foobar this is a comment",
		"object_library": "Chrome HTML",
		"window_name": "Hello world! – TTE Site Title",
		"xpath": "//article[@id='div-comment-4']/div[1]/p",
		"title": "Hello world! – TTE Site Title",
		"url": "http://localhost:8080/?p=1#comment-2",
		"fullpath": {
			"path": [
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 0,
					"text": "foobar this is a comment 1569341043"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "comment-content"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "article",
					"attributes": [
						{
							"name": "id",
							"value": "div-comment-4"
						},
						{
							"name": "class",
							"value": "comment-body"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "comment-4"
						},
						{
							"name": "class",
							"value": "comment byuser comment-author-wp_admin bypostauthor odd alt thread-odd thread-alt depth-1"
						}
					],
					"indexInParent": 1,
					"level": 3
				},
				{
					"tagName": "ol",
					"attributes": [
						{
							"name": "class",
							"value": "comment-list"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "comments"
						},
						{
							"name": "class",
							"value": "comments-area"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "main",
					"attributes": [
						{
							"name": "id",
							"value": "main"
						},
						{
							"name": "class",
							"value": "site-main"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "primary"
						},
						{
							"name": "class",
							"value": "content-area"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "class",
							"value": "site-content"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "site-inner"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "page"
						},
						{
							"name": "class",
							"value": "site"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "single single-post postid-1 single-format-standard logged-in admin-bar  customize-support"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 12
				}
			]
		},
		"screenshot": "Reports\\eb46c67e-96c8-cd1b-341f-82a4b3aee856.png"
	},
	"September_23__2019_at_9_32_pm": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "\n\t\t\t\t\t\t\t\tSeptember 23, 2019 at 9:32 pm\t\t\t\t\t\t\t",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "September 23, 2019 at 9:32 pm",
		"object_library": "Chrome HTML",
		"window_name": "Hello world! – TTE Site Title",
		"xpath": "//article[@id='div-comment-4']/footer/div[2]/a/time",
		"title": "Hello world! – TTE Site Title",
		"url": "http://localhost:8080/?p=1#comment-2",
		"fullpath": {
			"path": [
				{
					"tagName": "time",
					"attributes": [
						{
							"name": "datetime",
							"value": "2019-09-24T16:04:06+00:00"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "September 24, 2019 at 4:04 pm "
				},
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "http://localhost:8080/?p=1#comment-4"
						}
					],
					"indexInParent": 0,
					"level": 1,
					"text": "September 24, 2019 at 4:04 pm "
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "comment-metadata"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "footer",
					"attributes": [
						{
							"name": "class",
							"value": "comment-meta"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "article",
					"attributes": [
						{
							"name": "id",
							"value": "div-comment-4"
						},
						{
							"name": "class",
							"value": "comment-body"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "comment-4"
						},
						{
							"name": "class",
							"value": "comment byuser comment-author-wp_admin bypostauthor odd alt thread-odd thread-alt depth-1"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "ol",
					"attributes": [
						{
							"name": "class",
							"value": "comment-list"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "comments"
						},
						{
							"name": "class",
							"value": "comments-area"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "main",
					"attributes": [
						{
							"name": "id",
							"value": "main"
						},
						{
							"name": "class",
							"value": "site-main"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "primary"
						},
						{
							"name": "class",
							"value": "content-area"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "class",
							"value": "site-content"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "site-inner"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "page"
						},
						{
							"name": "class",
							"value": "site"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "single single-post postid-1 single-format-standard logged-in admin-bar  customize-support"
						}
					],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 14
				}
			]
		},
		"screenshot": "Reports\\00b0da85-1812-9448-71d3-378e7916de08.png"
	}
}