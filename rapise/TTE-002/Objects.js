var saved_script_objects={
	"Username_or_Email": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Username or Email\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTC Test Site ‹ Log In",
		"xpath": "//input[@name='log' and @id='user_login']",
		"title": "TTC Test Site ‹ Log In",
		"url": "http://localhost:8080/wp-login.php"
	},
	"Password": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Password\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTC Test Site ‹ Log In",
		"xpath": "//input[@name='pwd' and @id='user_pass']",
		"title": "TTC Test Site ‹ Log In",
		"url": "http://localhost:8080/wp-login.php"
	},
	"wp_submit": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "wp-submit",
		"object_library": "Chrome HTML",
		"window_name": "TTC Test Site ‹ Log In",
		"xpath": "//input[@name='wp-submit' and @id='wp-submit']",
		"title": "TTC Test Site ‹ Log In",
		"url": "http://localhost:8080/wp-login.php"
	},
	"Add_an_About_page": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Add an About page",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTC Test Site — WordPress",
		"xpath": "//div[@id='welcome-panel']/div/div/div[2]/ul/li[2]/a",
		"title": "Dashboard ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/"
	},
	"Enter_title_here": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Enter title here",
		"object_library": "Chrome HTML",
		"window_name": "Add New Page ‹ TTC Test Site — WordPress",
		"xpath": "//input[@name='post_title' and @id='title']",
		"title": "Add New Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php?post_type=page"
	},
	"Visual": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "Visual",
		"object_library": "Chrome HTML",
		"window_name": "Add New Page ‹ TTC Test Site — WordPress",
		"xpath": "//button[@id='content-tmce']",
		"title": "Add New Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php?post_type=page"
	},
	"HTML": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": ".mce-content-body div.mce-resizehandle {position: absolute;border: 1px solid black;box-sizing: box-sizing;background: #F...",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "HTML",
		"object_library": "Chrome HTML",
		"window_name": "Add New Page ‹ TTC Test Site — WordPress",
		"xpath": "//iframe[@id='content_ifr']@@@/html",
		"title": "Add New Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php?post_type=page"
	},
	"I": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "I",
		"object_library": "Chrome HTML",
		"window_name": "Add New Page ‹ TTC Test Site — WordPress",
		"xpath": "//div[@id='mceu_0']/button/i",
		"title": "Add New Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php?post_type=page"
	},
	"P": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "P",
		"object_library": "Chrome HTML",
		"window_name": "Add New Page ‹ TTC Test Site — WordPress",
		"xpath": "//iframe[@id='content_ifr']@@@//p[2]",
		"title": "Add New Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php?post_type=page"
	},
	"BUTTON": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "BUTTON",
		"object_library": "Chrome HTML",
		"window_name": "Add New Page ‹ TTC Test Site — WordPress",
		"xpath": "//div[@id='mceu_0']/button",
		"title": "Add New Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php?post_type=page"
	},
	"publish": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "publish",
		"object_library": "Chrome HTML",
		"window_name": "Add New Page ‹ TTC Test Site — WordPress",
		"xpath": "//input[@name='publish' and @id='publish']",
		"title": "Add New Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php?post_type=page"
	},
	"Published": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "\nPublished",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "Published",
		"object_library": "Chrome HTML",
		"window_name": "Edit Page ‹ TTC Test Site — WordPress",
		"xpath": "//span[@id='post-status-display']",
		"title": "Edit Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post.php?post=5&action=edit"
	},
	"View_Page": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "View Page",
		"object_library": "Chrome HTML",
		"window_name": "Edit Page ‹ TTC Test Site — WordPress",
		"xpath": "//li[@id='wp-admin-bar-view']/a",
		"title": "Edit Page ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/post.php?post=5&action=edit"
	},
	"Testing_Tool_Evaluation_Project_": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "Testing Tool Evaluation Project SUT",
		"object_library": "Chrome HTML",
		"window_name": "Testing Tool Evaluation Project SUT – TTC Test Site",
		"xpath": "//h1",
		"title": "Testing Tool Evaluation Project SUT – TTC Test Site",
		"url": "http://localhost:8080/?page_id=5"
	},
	"DIV": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "DIV",
		"object_library": "Chrome HTML",
		"window_name": "Testing Tool Evaluation Project SUT – TTC Test Site",
		"xpath": "//article[@id='post-5']/div",
		"title": "Testing Tool Evaluation Project SUT – TTC Test Site",
		"url": "http://localhost:8080/?page_id=5"
	},
	"This_is_bold_text_": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "This is bold text.",
		"object_library": "Chrome HTML",
		"window_name": "Testing Tool Evaluation Project SUT – TTC Test Site",
		"xpath": "//strong",
		"title": "Testing Tool Evaluation Project SUT – TTC Test Site",
		"url": "http://localhost:8080/?page_id=5"
	},
	"Howdy__wp_admin": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Howdy, wp_admin",
		"object_library": "Chrome HTML",
		"window_name": "Testing Tool Evaluation Project SUT – TTC Test Site",
		"xpath": "//li[@id='wp-admin-bar-my-account']/a",
		"title": "Testing Tool Evaluation Project SUT – TTC Test Site",
		"url": "http://localhost:8080/?page_id=5"
	},
	"Log_Out": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Log Out",
		"object_library": "Chrome HTML",
		"window_name": "Profile ‹ TTC Test Site — WordPress",
		"xpath": "//li[@id='wp-admin-bar-logout']/a",
		"title": "Profile ‹ TTC Test Site — WordPress",
		"url": "http://localhost:8080/wp-admin/profile.php"
	}
};