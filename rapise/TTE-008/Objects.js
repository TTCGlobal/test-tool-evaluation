var saved_script_objects={
	"Username_or_Email": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "TTE Site Title ‹ Log In - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Username or Email\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='log' and @id='user_login']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "text"
						},
						{
							"name": "name",
							"value": "log"
						},
						{
							"name": "id",
							"value": "user_login"
						},
						{
							"name": "aria-describedby",
							"value": "login_error"
						},
						{
							"name": "class",
							"value": "input"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "size",
							"value": "20"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "user_login"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\6cd6f3ac-d92a-2b88-683c-60b046f5d180.png"
	},
	"Password": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "TTE Site Title ‹ Log In - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Password\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='pwd' and @id='user_pass']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "password"
						},
						{
							"name": "name",
							"value": "pwd"
						},
						{
							"name": "id",
							"value": "user_pass"
						},
						{
							"name": "aria-describedby",
							"value": "login_error"
						},
						{
							"name": "class",
							"value": "input"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "size",
							"value": "20"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "user_pass"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\da5ca1b0-27df-66cf-bcb7-9fbf447befaa.png"
	},
	"wp_submit": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "TTE Site Title ‹ Log In - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "wp-submit",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='wp-submit' and @id='wp-submit']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "name",
							"value": "wp-submit"
						},
						{
							"name": "id",
							"value": "wp-submit"
						},
						{
							"name": "class",
							"value": "button button-primary button-large"
						},
						{
							"name": "value",
							"value": "Log In"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "submit"
						}
					],
					"indexInParent": 3,
					"level": 1
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 5
				}
			]
		},
		"screenshot": "Recording\\d26a6fd5-b858-4efc-2252-59c8d2da6dbc.png"
	},
	"_New": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Dashboard ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "New",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-new-content']/a",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/post-new.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "New"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-content"
						},
						{
							"name": "class",
							"value": "menupop hover"
						}
					],
					"indexInParent": 5,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-root-default"
						},
						{
							"name": "class",
							"value": "ab-top-menu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Recording\\917b5ffb-b07b-9279-8f3a-765d00fc1fe8.png"
	},
	"Enter_title_here": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Add New Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Enter title here",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='post_title' and @id='title']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "text"
						},
						{
							"name": "name",
							"value": "post_title"
						},
						{
							"name": "size",
							"value": "30"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "id",
							"value": "title"
						},
						{
							"name": "spellcheck",
							"value": "true"
						},
						{
							"name": "autocomplete",
							"value": "off"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "titlewrap"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "titlediv"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 13
				}
			]
		},
		"screenshot": "Reports\\fe55b039-148d-3ab3-4bdb-812a2d51ec4b.png"
	},
	"wpbody_content": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Add New Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "wpbody-content",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//div[@id='wpbody-content']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 5
				}
			]
		},
		"screenshot": "Recording\\626a6d7d-b488-cf31-d9d9-4d883574ef4b.png"
	},
	"Add_Media": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Add New Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "Add Media",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//button[@id='insert-media-button']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "button",
					"attributes": [
						{
							"name": "type",
							"value": "button"
						},
						{
							"name": "id",
							"value": "insert-media-button"
						},
						{
							"name": "class",
							"value": "button insert-media add_media"
						},
						{
							"name": "data-editor",
							"value": "content"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-media-buttons"
						},
						{
							"name": "class",
							"value": "wp-media-buttons"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-editor-tools"
						},
						{
							"name": "class",
							"value": "wp-editor-tools hide-if-no-js"
						},
						{
							"name": "style",
							"value": "position: absolute; top: 0px; width: 1401px;"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-wrap"
						},
						{
							"name": "class",
							"value": "wp-core-ui wp-editor-wrap html-active has-dfw"
						},
						{
							"name": "style",
							"value": "padding-top: 55px;"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postdivrich"
						},
						{
							"name": "class",
							"value": "postarea wp-editor-expand"
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 12
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 14
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 15
				}
			]
		},
		"screenshot": "Recording\\ec190ae5-54d5-b5de-f5a4-1ae37937c1a2.png"
	},
	"Deselect": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Add New Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "Deselect",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//ul[@id='__attachments-view-100']/li",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "role",
							"value": "checkbox"
						},
						{
							"name": "aria-label",
							"value": "image"
						},
						{
							"name": "aria-checked",
							"value": "false"
						},
						{
							"name": "data-id",
							"value": "30"
						},
						{
							"name": "class",
							"value": "attachment save-ready"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "tabindex",
							"value": "-1"
						},
						{
							"name": "class",
							"value": "attachments ui-sortable ui-sortable-disabled"
						},
						{
							"name": "id",
							"value": "__attachments-view-100"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "attachments-browser"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame-content"
						},
						{
							"name": "data-columns",
							"value": "9"
						}
					],
					"indexInParent": 3,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame mode-select wp-core-ui"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal-content"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal wp-core-ui"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-2"
						},
						{
							"name": "class",
							"value": "supports-drag-drop"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 4,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Recording\\735f7830-b2bd-df0b-995e-013c9683b001.png"
	},
	"Insert_into_post": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Add New Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "Insert into post",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//div[5]/div/div[2]/button",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "button",
					"attributes": [
						{
							"name": "type",
							"value": "button"
						},
						{
							"name": "class",
							"value": "button media-button button-primary button-large media-button-insert"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Insert into post"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-toolbar-primary search-form"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-toolbar"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame-toolbar"
						}
					],
					"indexInParent": 4,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame mode-select wp-core-ui"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal-content"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal wp-core-ui"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-2"
						},
						{
							"name": "class",
							"value": "supports-drag-drop"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 4,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Recording\\7a73fee7-5630-90a6-cc59-e090ca9d3668.png"
	},
	"Text": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Add New Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "Text",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//button[@id='content-html']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "button",
					"attributes": [
						{
							"name": "type",
							"value": "button"
						},
						{
							"name": "id",
							"value": "content-html"
						},
						{
							"name": "class",
							"value": "wp-switch-editor switch-html"
						},
						{
							"name": "data-wp-editor-id",
							"value": "content"
						}
					],
					"indexInParent": 1,
					"level": 0,
					"text": "Text"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wp-editor-tabs"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-editor-tools"
						},
						{
							"name": "class",
							"value": "wp-editor-tools hide-if-no-js"
						},
						{
							"name": "style",
							"value": "position: absolute; top: 0px; width: 1401px;"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-wrap"
						},
						{
							"name": "class",
							"value": "wp-core-ui wp-editor-wrap html-active has-dfw"
						},
						{
							"name": "style",
							"value": "padding-top: 55px;"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postdivrich"
						},
						{
							"name": "class",
							"value": "postarea wp-editor-expand"
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 12
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 14
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 15
				}
			]
		},
		"screenshot": "Recording\\3c89fcef-54c6-2cf4-fa06-f163ec8182a9.png"
	},
	"content": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "content",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//textarea[@name='content' and @id='content']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "textarea",
					"attributes": [
						{
							"name": "class",
							"value": "wp-editor-area"
						},
						{
							"name": "style",
							"value": "height: 320px; margin-top: 37px;"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "cols",
							"value": "40"
						},
						{
							"name": "name",
							"value": "content"
						},
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "aria-hidden",
							"value": "false"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-editor-container"
						},
						{
							"name": "class",
							"value": "wp-editor-container"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-wrap"
						},
						{
							"name": "class",
							"value": "wp-core-ui wp-editor-wrap html-active has-dfw"
						},
						{
							"name": "style",
							"value": "padding-top: 55px;"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postdivrich"
						},
						{
							"name": "class",
							"value": "postarea wp-editor-expand"
						}
					],
					"indexInParent": 1,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 14
				}
			]
		},
		"screenshot": "Recording\\90c40b81-709b-acb6-7360-031c5061e901.png"
	},
	"publish": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Add New Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "publish",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='publish' and @id='publish']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "name",
							"value": "publish"
						},
						{
							"name": "id",
							"value": "publish"
						},
						{
							"name": "class",
							"value": "button button-primary button-large"
						},
						{
							"name": "value",
							"value": "Publish"
						}
					],
					"indexInParent": 1,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "publishing-action"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "major-publishing-actions"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "submitbox"
						},
						{
							"name": "id",
							"value": "submitpost"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "inside"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "submitdiv"
						},
						{
							"name": "class",
							"value": "postbox "
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "side-sortables"
						},
						{
							"name": "class",
							"value": "meta-box-sortables ui-sortable"
						},
						{
							"name": "style",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postbox-container-1"
						},
						{
							"name": "class",
							"value": "postbox-container"
						}
					],
					"indexInParent": 1,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 13
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 14
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 15
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 16
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 17
				}
			]
		},
		"screenshot": "Recording\\985d2d19-f2b6-2a72-6017-39bbd331ea54.png"
	},
	"Published": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "\nPublished",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "Published",
		"object_library": "Chrome HTML",
		"window_name": "Edit Post ‹ TTE Site Title — WordPress",
		"xpath": "//span[@id='post-status-display']",
		"title": "Edit Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post.php?post=36&action=edit",
		"fullpath": {
			"path": [
				{
					"tagName": "span",
					"attributes": [
						{
							"name": "id",
							"value": "post-status-display"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Published"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "misc-pub-section misc-pub-post-status"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "misc-publishing-actions"
						}
					],
					"indexInParent": 2,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "minor-publishing"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "submitbox"
						},
						{
							"name": "id",
							"value": "submitpost"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "inside"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "submitdiv"
						},
						{
							"name": "class",
							"value": "postbox "
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "side-sortables"
						},
						{
							"name": "class",
							"value": "meta-box-sortables ui-sortable"
						},
						{
							"name": "style",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postbox-container-1"
						},
						{
							"name": "class",
							"value": "postbox-container"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 12
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 14
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 15
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 16
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 17
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 18
				}
			]
		},
		"screenshot": "Reports\\acba3602-dbf5-b6e4-6bfb-12e14cd20710.png"
	},
	"View_post": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Edit Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "View post",
		"object_library": "Chrome HTML",
		"window_name": "Edit Post ‹ TTE Site Title — WordPress",
		"xpath": "//div[@id='message']/p/a",
		"title": "Edit Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post.php?post=36&action=edit",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "http://localhost:8080/?p=36"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "View post"
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "message"
						},
						{
							"name": "class",
							"value": "updated notice notice-success is-dismissible"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Recording\\64f04d34-b777-b668-216e-82bb3a758988.png"
	},
	"image": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Image",
		"object_name": "image",
		"object_library": "Chrome HTML",
		"window_name": "Image Blog Post – TTE Site Title",
		"xpath": "//p/img",
		"title": "Image Blog Post – TTE Site Title",
		"url": "http://localhost:8080/?p=36",
		"fullpath": {
			"path": [
				{
					"tagName": "img",
					"attributes": [
						{
							"name": "src",
							"value": "http://localhost:8080/wp-content/uploads/2019/09/image-1-300x221.png"
						},
						{
							"name": "alt",
							"value": "image"
						},
						{
							"name": "width",
							"value": "300"
						},
						{
							"name": "height",
							"value": "221"
						},
						{
							"name": "class",
							"value": "alignnone size-medium wp-image-30"
						},
						{
							"name": "sizes",
							"value": "(max-width: 300px) 85vw, 300px"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "entry-content"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "article",
					"attributes": [
						{
							"name": "id",
							"value": "post-36"
						},
						{
							"name": "class",
							"value": "post-36 post type-post status-publish format-standard hentry category-uncategorized"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "main",
					"attributes": [
						{
							"name": "id",
							"value": "main"
						},
						{
							"name": "class",
							"value": "site-main"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "primary"
						},
						{
							"name": "class",
							"value": "content-area"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "class",
							"value": "site-content"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "site-inner"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "page"
						},
						{
							"name": "class",
							"value": "site"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "single single-post postid-36 single-format-standard logged-in admin-bar  customize-support"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 10
				}
			]
		},
		"screenshot": "Recording\\d01d3086-1191-8bff-4262-7b13908acb6c.png"
	},
	"Howdy__wp_admin": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Image Blog Post – TTE Site Title - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Howdy, wp_admin",
		"object_library": "Chrome HTML",
		"window_name": "Image Blog Post – TTE Site Title",
		"xpath": "//li[@id='wp-admin-bar-my-account']/a",
		"title": "Image Blog Post – TTE Site Title",
		"url": "http://localhost:8080/?p=36",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/profile.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Howdy, wp_admin"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-my-account"
						},
						{
							"name": "class",
							"value": "menupop with-avatar hover"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-top-secondary"
						},
						{
							"name": "class",
							"value": "ab-top-secondary ab-top-menu"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "single single-post postid-36 single-format-standard logged-in admin-bar  customize-support"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\36440c8a-e63e-0cd6-7013-eb6f27fc8840.png"
	},
	"Log_Out": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Profile ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Log Out",
		"object_library": "Chrome HTML",
		"window_name": "Profile ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-logout']/a",
		"title": "Profile ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/profile.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-login.php?action=logout&_wpnonce=59ccdcf5e6"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Log Out"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-logout"
						}
					],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-user-actions"
						},
						{
							"name": "class",
							"value": "ab-submenu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "ab-sub-wrapper"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-my-account"
						},
						{
							"name": "class",
							"value": "menupop with-avatar hover"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-top-secondary"
						},
						{
							"name": "class",
							"value": "ab-top-secondary ab-top-menu"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\7ffdd9b3-4622-2368-8351-2d5124faccf6.png"
	},
	"You_are_now_logged_out_": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "\tYou are now logged out.\n",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "You are now logged out.",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//div[@id='login']/p[1]",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "message"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				}
			]
		},
		"screenshot": "Recording\\999955d3-c0fd-bf9c-f5cb-8c5f39f522f2.png"
	},
	"DIV": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "\n\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\n\t\t\t",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "DIV",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//ul[@id='__attachments-view-100']/li/div/div",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "thumbnail"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "attachment-preview js--select-attachment type-image subtype-png landscape"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "role",
							"value": "checkbox"
						},
						{
							"name": "aria-label",
							"value": "image"
						},
						{
							"name": "aria-checked",
							"value": "false"
						},
						{
							"name": "data-id",
							"value": "30"
						},
						{
							"name": "class",
							"value": "attachment save-ready"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "tabindex",
							"value": "-1"
						},
						{
							"name": "class",
							"value": "attachments ui-sortable ui-sortable-disabled"
						},
						{
							"name": "id",
							"value": "__attachments-view-100"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "attachments-browser"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame-content"
						},
						{
							"name": "data-columns",
							"value": "9"
						}
					],
					"indexInParent": 3,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame mode-select wp-core-ui"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal-content"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal wp-core-ui"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-2"
						},
						{
							"name": "class",
							"value": "supports-drag-drop"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 4,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\f34649fb-e9ed-37b8-d547-887c7f6bf000.png"
	},
	"Media_Library": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "MozillaWindowClass",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Media Library",
		"object_library": "Firefox HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//div[3]/div/a[2]",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "#"
						},
						{
							"name": "class",
							"value": "media-menu-item"
						}
					],
					"indexInParent": 1,
					"level": 0,
					"text": "Media Library"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-router"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame-router"
						}
					],
					"indexInParent": 2,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame mode-select wp-core-ui"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal-content"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal wp-core-ui"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-2"
						},
						{
							"name": "style",
							"value": "position: relative;"
						},
						{
							"name": "class",
							"value": "supports-drag-drop"
						}
					],
					"indexInParent": 4,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Recording\\9aa0ea13-591c-8f81-4670-17c8928d3916.png"
	},
	"Deselect1": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "\n\t\t\n\t\t\t\n\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\n\t\t\t\n\t\t\t\n\t\t\n\t\t\n\t\t\tDeselect\n\t\t\n\t\t\n\t",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "MozillaWindowClass",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "Deselect",
		"object_library": "Firefox HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//ul[@id='__attachments-view-105']/li[1]",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "role",
							"value": "checkbox"
						},
						{
							"name": "aria-label",
							"value": "image"
						},
						{
							"name": "aria-checked",
							"value": "false"
						},
						{
							"name": "data-id",
							"value": "22"
						},
						{
							"name": "class",
							"value": "attachment save-ready"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "tabindex",
							"value": "-1"
						},
						{
							"name": "class",
							"value": "attachments ui-sortable ui-sortable-disabled"
						},
						{
							"name": "id",
							"value": "__attachments-view-105"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "attachments-browser"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame-content"
						},
						{
							"name": "data-columns",
							"value": "5"
						}
					],
					"indexInParent": 3,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame mode-select wp-core-ui"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal-content"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal wp-core-ui"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-2"
						},
						{
							"name": "style",
							"value": "position: relative;"
						},
						{
							"name": "class",
							"value": "supports-drag-drop"
						}
					],
					"indexInParent": 4,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Recording\\f3b75877-b64d-ddd7-c40f-e487b4cc65e2.png"
	}
};