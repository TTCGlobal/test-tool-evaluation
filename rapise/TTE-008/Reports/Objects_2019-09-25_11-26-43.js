var saved_script_objects = {
	"Published": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "\nPublished",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "Published",
		"object_library": "Chrome HTML",
		"window_name": "Edit Post ‹ TTE Site Title — WordPress",
		"xpath": "//span[@id='post-status-display']",
		"title": "Edit Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post.php?post=36&action=edit",
		"fullpath": {
			"path": [
				{
					"tagName": "span",
					"attributes": [
						{
							"name": "id",
							"value": "post-status-display"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Published"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "misc-pub-section misc-pub-post-status"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "misc-publishing-actions"
						}
					],
					"indexInParent": 2,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "minor-publishing"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "submitbox"
						},
						{
							"name": "id",
							"value": "submitpost"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "inside"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "submitdiv"
						},
						{
							"name": "class",
							"value": "postbox "
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "side-sortables"
						},
						{
							"name": "class",
							"value": "meta-box-sortables ui-sortable"
						},
						{
							"name": "style",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postbox-container-1"
						},
						{
							"name": "class",
							"value": "postbox-container"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 12
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 14
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 15
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 16
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 17
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 18
				}
			]
		},
		"screenshot": "Reports\\acba3602-dbf5-b6e4-6bfb-12e14cd20710.png"
	}
}