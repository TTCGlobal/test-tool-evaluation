var saved_script_objects={
	"Username_or_Email": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Username or Email\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='log' and @id='user_login']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "text"
						},
						{
							"name": "name",
							"value": "log"
						},
						{
							"name": "id",
							"value": "user_login"
						},
						{
							"name": "class",
							"value": "input"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "size",
							"value": "20"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "user_login"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\987e1f4d-fc69-b0f7-191b-d7098fe92650.png"
	},
	"login": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "login",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//div[@id='login']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 2
				}
			]
		},
		"screenshot": "Recording\\d4dc5782-92ad-dedd-5e23-52fd7783eca3.png"
	},
	"Password": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Password\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='pwd' and @id='user_pass']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "password"
						},
						{
							"name": "name",
							"value": "pwd"
						},
						{
							"name": "id",
							"value": "user_pass"
						},
						{
							"name": "class",
							"value": "input"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "size",
							"value": "20"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "user_pass"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\0816584e-e165-a0f9-21b3-d05fc7cae9b2.png"
	},
	"wp_submit": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "wp-submit",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='wp-submit' and @id='wp-submit']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "name",
							"value": "wp-submit"
						},
						{
							"name": "id",
							"value": "wp-submit"
						},
						{
							"name": "class",
							"value": "button button-primary button-large"
						},
						{
							"name": "value",
							"value": "Log In"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "submit"
						}
					],
					"indexInParent": 3,
					"level": 1
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 5
				}
			]
		},
		"screenshot": "Recording\\95b08fbb-3067-2b08-20ed-f8a7ca3989f7.png"
	},
	"Media": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "Media",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='menu-media']/a/div[3]",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wp-menu-name"
						}
					],
					"indexInParent": 2,
					"level": 0,
					"text": "Media"
				},
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "upload.php"
						},
						{
							"name": "class",
							"value": "wp-has-submenu wp-not-current-submenu menu-top menu-icon-media"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						}
					],
					"indexInParent": 0,
					"level": 1,
					"text": "\n\nMedia"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "class",
							"value": "wp-has-submenu wp-not-current-submenu menu-top menu-icon-media opensub"
						},
						{
							"name": "id",
							"value": "menu-media"
						}
					],
					"indexInParent": 3,
					"level": 2
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenu"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenuwrap"
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenumain"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Main menu"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Recording\\0788ea98-9004-9ef8-6eb9-c4237aa2d553.png"
	},
	"Add_New": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Add New",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='menu-media']/ul/li[3]/a",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "media-new.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Add New"
				},
				{
					"tagName": "li",
					"attributes": [],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "class",
							"value": "wp-submenu wp-submenu-wrap"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "class",
							"value": "wp-has-submenu wp-not-current-submenu menu-top menu-icon-media opensub"
						},
						{
							"name": "id",
							"value": "menu-media"
						}
					],
					"indexInParent": 3,
					"level": 3
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenu"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenuwrap"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenumain"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Main menu"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Recording\\481a8ca9-35c2-4f75-08fe-f364bc0204fc.png"
	},
	"Upload": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "MozillaWindowClass",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "plupload-browse-button",
		"object_library": "Firefox HTML",
		"window_name": "Upload New Media ‹ TTE Site Title — WordPress",
		"xpath": "//input[@id='plupload-browse-button']",
		"title": "Upload New Media ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/media-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "id",
							"value": "plupload-browse-button"
						},
						{
							"name": "type",
							"value": "button"
						},
						{
							"name": "value",
							"value": "Select Files"
						},
						{
							"name": "class",
							"value": "button"
						},
						{
							"name": "style",
							"value": "position: relative; z-index: 1;"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "drag-drop-buttons"
						}
					],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "drag-drop-inside"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "drag-drop-area"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "plupload-upload-ui"
						},
						{
							"name": "class",
							"value": "hide-if-no-js drag-drop"
						}
					],
					"indexInParent": 2,
					"level": 4
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "enctype",
							"value": "multipart/form-data"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-admin/media-new.php"
						},
						{
							"name": "class",
							"value": "media-upload-form type-form validate"
						},
						{
							"name": "id",
							"value": "file-form"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						}
					],
					"indexInParent": 0,
					"level": 12
				}
			]
		},
		"screenshot": "Recording\\9b96ae45-e8cd-673a-2149-e75eb787ba0d.png"
	},
	"wpbody_content": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "wpbody-content",
		"object_library": "Chrome HTML",
		"window_name": "Upload New Media ‹ TTE Site Title — WordPress",
		"xpath": "//div[@id='wpbody-content']",
		"title": "Upload New Media ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/media-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 5
				}
			]
		},
		"screenshot": "Recording\\a02cefa4-13da-13b8-347b-87507d5ab7a7.png"
	},
	"image": {
		"locations": [
			{
				"locator_name": "Location",
				"location": {
					"location": "4.1.4.1.4.2.3.4.1.4.1",
					"window_name": "Open",
					"window_class": "#32770"
				}
			},
			{
				"locator_name": "LocationPath",
				"location": {
					"window_name": "Open",
					"window_class": "#32770",
					"path": [
						{
							"object_name": "param:object_name",
							"object_class": "param:object_class",
							"object_role": "param:object_role"
						},
						{
							"object_name": "Items View",
							"object_class": "param:object_class",
							"object_role": "ROLE_SYSTEM_LIST"
						},
						{
							"object_name": "Items View",
							"object_class": "param:object_class",
							"object_role": "ROLE_SYSTEM_WINDOW"
						},
						{
							"object_name": "Shell Folder View",
							"object_class": "SHELLDLL_DefView",
							"object_role": "ROLE_SYSTEM_CLIENT"
						},
						{
							"object_name": "Shell Folder View",
							"object_class": "SHELLDLL_DefView",
							"object_role": "ROLE_SYSTEM_WINDOW"
						},
						{
							"object_name": "Folder Layout Pane",
							"object_class": "param:object_class",
							"object_role": "ROLE_SYSTEM_PANE"
						},
						{
							"object_name": "Explorer Pane",
							"object_class": "param:object_class",
							"object_role": "ROLE_SYSTEM_PANE"
						},
						{
							"object_name": "Explorer Pane",
							"object_class": "param:object_class",
							"object_role": "ROLE_SYSTEM_WINDOW"
						},
						{
							"object_name": "",
							"object_class": "DUIViewWndClassName",
							"object_role": "ROLE_SYSTEM_CLIENT"
						},
						{
							"object_name": "",
							"object_class": "DUIViewWndClassName",
							"object_role": "ROLE_SYSTEM_WINDOW"
						},
						{
							"object_name": "Open",
							"object_class": "#32770",
							"object_role": "ROLE_SYSTEM_DIALOG"
						}
					]
				}
			},
			{
				"locator_name": "LocationRect",
				"location": {
					"window_name": "Open",
					"window_class": "#32770",
					"rect": {
						"object_name": "param:object_name",
						"object_class": "param:object_class",
						"object_role": "param:object_role",
						"x": 182,
						"y": 103,
						"w": 105,
						"h": 93
					}
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "",
		"object_role": "ROLE_SYSTEM_LISTITEM",
		"object_class": "DirectUIHWND",
		"version": 0,
		"object_type": "SeSSimulated",
		"object_flavor": "Simulated",
		"object_name": "image",
		"window_name": "Upload New Media ‹ TTE Site Title — WordPress - Google Chrome"
	},
	"Open": {
		"locations": [
			{
				"locator_name": "Location",
				"location": {
					"location": "4.6.4",
					"window_name": "param:object_name",
					"window_class": "#32770"
				}
			},
			{
				"locator_name": "LocationPath",
				"location": {
					"window_name": "param:object_name",
					"window_class": "#32770",
					"path": [
						{
							"object_name": "param:object_name",
							"object_class": "param:object_class",
							"object_role": "param:object_role"
						},
						{
							"object_name": "param:object_name",
							"object_class": "param:object_class",
							"object_role": "ROLE_SYSTEM_WINDOW"
						},
						{
							"object_name": "param:object_name",
							"object_class": "#32770",
							"object_role": "ROLE_SYSTEM_DIALOG"
						}
					]
				}
			},
			{
				"locator_name": "LocationRect",
				"location": {
					"window_name": "param:object_name",
					"window_class": "#32770",
					"rect": {
						"object_name": "param:object_name",
						"object_class": "param:object_class",
						"object_role": "param:object_role",
						"x": 747,
						"y": 494,
						"w": 88,
						"h": 26
					}
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "&Open",
		"object_role": "ROLE_SYSTEM_PUSHBUTTON",
		"object_class": "Button",
		"version": 0,
		"object_type": "SeSSimulated",
		"object_flavor": "Simulated",
		"object_name": "Open",
		"window_name": "Upload New Media ‹ TTE Site Title — WordPress - Google Chrome"
	},
	"DIV": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "\n\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\n\t\t\t",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "DIV",
		"object_library": "Chrome HTML",
		"window_name": "Media Library ‹ TTE Site Title — WordPress",
		"xpath": "//ul[@id='__attachments-view-42']/li/div/div",
		"title": "Media Library ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/upload.php",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "thumbnail"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "attachment-preview js--select-attachment type-image subtype-png landscape"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "role",
							"value": "checkbox"
						},
						{
							"name": "aria-label",
							"value": "image"
						},
						{
							"name": "aria-checked",
							"value": "false"
						},
						{
							"name": "data-id",
							"value": "28"
						},
						{
							"name": "class",
							"value": "attachment save-ready"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "tabindex",
							"value": "-1"
						},
						{
							"name": "class",
							"value": "attachments ui-sortable ui-sortable-disabled"
						},
						{
							"name": "id",
							"value": "__attachments-view-42"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "attachments-browser hide-sidebar sidebar-for-errors"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame-content"
						},
						{
							"name": "data-columns",
							"value": "11"
						}
					],
					"indexInParent": 3,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame wp-core-ui mode-grid mode-edit hide-menu"
						}
					],
					"indexInParent": 1,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						},
						{
							"name": "id",
							"value": "wp-media-grid"
						},
						{
							"name": "data-search",
							"value": ""
						}
					],
					"indexInParent": 3,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 13
				}
			]
		},
		"screenshot": "Recording\\7cd719f1-9c25-b4f4-0cbe-71be9c752312.png"
	},
	"_New": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "New",
		"object_library": "Chrome HTML",
		"window_name": "Media Library ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-new-content']/a/span[2]",
		"title": "Media Library ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/upload.php",
		"fullpath": {
			"path": [
				{
					"tagName": "span",
					"attributes": [
						{
							"name": "class",
							"value": "ab-label"
						}
					],
					"indexInParent": 1,
					"level": 0,
					"text": "New"
				},
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/post-new.php"
						}
					],
					"indexInParent": 0,
					"level": 1,
					"text": "New"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-content"
						},
						{
							"name": "class",
							"value": "menupop hover"
						}
					],
					"indexInParent": 5,
					"level": 2
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-root-default"
						},
						{
							"name": "class",
							"value": "ab-top-menu"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Recording\\8ba2dcec-798c-daf3-55d2-4df4bd8cf922.png"
	},
	"Post": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Post",
		"object_library": "Chrome HTML",
		"window_name": "Media Library ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-new-post']/a",
		"title": "Media Library ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/upload.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/post-new.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Post"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-post"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-content-default"
						},
						{
							"name": "class",
							"value": "ab-submenu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "ab-sub-wrapper"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-new-content"
						},
						{
							"name": "class",
							"value": "menupop hover"
						}
					],
					"indexInParent": 5,
					"level": 4
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-root-default"
						},
						{
							"name": "class",
							"value": "ab-top-menu"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\c0aaa84e-6351-488c-b55d-850a22623669.png"
	},
	"Add_Media": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "Add Media",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//button[@id='insert-media-button']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "button",
					"attributes": [
						{
							"name": "type",
							"value": "button"
						},
						{
							"name": "id",
							"value": "insert-media-button"
						},
						{
							"name": "class",
							"value": "button insert-media add_media"
						},
						{
							"name": "data-editor",
							"value": "content"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-media-buttons"
						},
						{
							"name": "class",
							"value": "wp-media-buttons"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-editor-tools"
						},
						{
							"name": "class",
							"value": "wp-editor-tools hide-if-no-js"
						},
						{
							"name": "style",
							"value": "position: absolute; top: 0px; width: 1401px;"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wp-content-wrap"
						},
						{
							"name": "class",
							"value": "wp-core-ui wp-editor-wrap html-active has-dfw"
						},
						{
							"name": "style",
							"value": "padding-top: 55px;"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "postdivrich"
						},
						{
							"name": "class",
							"value": "postarea wp-editor-expand"
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 12
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 14
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 15
				}
			]
		},
		"screenshot": "Recording\\9c7d33ea-967d-6faf-e479-020faf0b702d.png"
	},
	"DIV1": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "\n\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\n\t\t\t\t\n\t\t\t",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "DIV",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//ul[@id='__attachments-view-100']/li/div/div",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "thumbnail"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "attachment-preview js--select-attachment type-image subtype-png landscape"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "role",
							"value": "checkbox"
						},
						{
							"name": "aria-label",
							"value": "image"
						},
						{
							"name": "aria-checked",
							"value": "false"
						},
						{
							"name": "data-id",
							"value": "28"
						},
						{
							"name": "class",
							"value": "attachment save-ready"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "tabindex",
							"value": "-1"
						},
						{
							"name": "class",
							"value": "attachments ui-sortable ui-sortable-disabled"
						},
						{
							"name": "id",
							"value": "__attachments-view-100"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "attachments-browser"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame-content"
						},
						{
							"name": "data-columns",
							"value": "9"
						}
					],
					"indexInParent": 3,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-frame mode-select wp-core-ui"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-0"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal-content"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal wp-core-ui"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-2"
						},
						{
							"name": "class",
							"value": "supports-drag-drop"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 4,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\b3971f8b-3d29-5fd6-2425-956be92df6e1.png"
	},
	"Close_media_panel": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "Close media panel",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//div[@id='__wp-uploader-id-2']/div[1]/button",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "button",
					"attributes": [
						{
							"name": "type",
							"value": "button"
						},
						{
							"name": "class",
							"value": "button-link media-modal-close"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "media-modal wp-core-ui"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "id",
							"value": "__wp-uploader-id-2"
						},
						{
							"name": "class",
							"value": "supports-drag-drop"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 4,
					"level": 2
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 4
				}
			]
		},
		"screenshot": "Recording\\bff19e07-52b8-c5b0-ce3d-59b3a0cc29b6.png"
	},
	"Howdy__wp_admin": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Howdy, wp_admin",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-my-account']/a",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/profile.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Howdy, wp_admin"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-my-account"
						},
						{
							"name": "class",
							"value": "menupop with-avatar hover"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-top-secondary"
						},
						{
							"name": "class",
							"value": "ab-top-secondary ab-top-menu"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Recording\\6db24a16-5178-6cce-6221-36c4ab0008a8.png"
	},
	"Log_Out": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Log Out",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-logout']/a",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-login.php?action=logout&_wpnonce=4e7b6005aa"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Log Out"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-logout"
						}
					],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-user-actions"
						},
						{
							"name": "class",
							"value": "ab-submenu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "ab-sub-wrapper"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-my-account"
						},
						{
							"name": "class",
							"value": "menupop with-avatar hover"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-top-secondary"
						},
						{
							"name": "class",
							"value": "ab-top-secondary ab-top-menu"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\f4001dea-caa9-cc58-3df7-9c1bef87ae16.png"
	},
	"You_are_now_logged_out_": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "\tYou are now logged out.\n",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "You are now logged out.",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//div[@id='login']/p[1]",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "message"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				}
			]
		},
		"screenshot": "Recording\\caf8c691-49e6-67c5-759a-cb794a4598d8.png"
	},
	"html_upload": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "html-upload",
		"object_library": "Chrome HTML",
		"window_name": "Upload New Media ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='html-upload' and @id='html-upload']",
		"title": "Upload New Media ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/media-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "name",
							"value": "html-upload"
						},
						{
							"name": "id",
							"value": "html-upload"
						},
						{
							"name": "class",
							"value": "button button-primary"
						},
						{
							"name": "value",
							"value": "Upload"
						}
					],
					"indexInParent": 1,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "id",
							"value": "async-upload-wrap"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "html-upload-ui"
						},
						{
							"name": "class",
							"value": "hide-if-js"
						}
					],
					"indexInParent": 3,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "enctype",
							"value": "multipart/form-data"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-admin/media-new.php"
						},
						{
							"name": "class",
							"value": "media-upload-form type-form validate html-uploader"
						},
						{
							"name": "id",
							"value": "file-form"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 10
				}
			]
		},
		"screenshot": "Recording\\553f43e4-3421-b012-0234-d038a2b0c28f.png"
	},
	"Google_Chrome___1_running_window": {
		"locations": [
			{
				"locator_name": "Location",
				"location": {
					"location": "4.5.4.1.4.1.4.31",
					"window_name": "",
					"window_class": "param:window_class"
				}
			},
			{
				"locator_name": "LocationPath",
				"location": {
					"window_name": "",
					"window_class": "param:window_class",
					"path": [
						{
							"object_name": "param:object_name",
							"object_class": "param:object_class",
							"object_role": "param:object_role"
						},
						{
							"object_name": "param:object_text",
							"object_class": "param:object_class",
							"object_role": "ROLE_SYSTEM_TOOLBAR"
						},
						{
							"object_name": "param:object_text",
							"object_class": "param:object_class",
							"object_role": "ROLE_SYSTEM_WINDOW"
						},
						{
							"object_name": "param:object_text",
							"object_class": "MSTaskSwWClass",
							"object_role": "ROLE_SYSTEM_CLIENT"
						},
						{
							"object_name": "param:object_text",
							"object_class": "MSTaskSwWClass",
							"object_role": "ROLE_SYSTEM_WINDOW"
						},
						{
							"object_name": "",
							"object_class": "ReBarWindow32",
							"object_role": "ROLE_SYSTEM_CLIENT"
						},
						{
							"object_name": "",
							"object_class": "ReBarWindow32",
							"object_role": "ROLE_SYSTEM_WINDOW"
						},
						{
							"object_name": "Taskbar",
							"object_class": "param:window_class",
							"object_role": "ROLE_SYSTEM_CLIENT"
						}
					]
				}
			},
			{
				"locator_name": "LocationRect",
				"location": {
					"window_name": "",
					"window_class": "param:window_class",
					"rect": {
						"object_name": "param:object_name",
						"object_class": "param:object_class",
						"object_role": "param:object_role",
						"x": 1178,
						"y": 0,
						"w": 49,
						"h": 40
					}
				}
			}
		],
		"window_class": "Shell_TrayWnd",
		"object_text": "Running applications",
		"object_role": "ROLE_SYSTEM_PUSHBUTTON",
		"object_class": "MSTaskListWClass",
		"version": 0,
		"object_type": "SeSSimulated",
		"object_flavor": "Simulated",
		"object_name": "Google Chrome - 1 running window",
		"window_name": ""
	}
};