var saved_script_objects = {
	"language_continue": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "language-continue",
		"object_library": "Chrome HTML",
		"window_name": "WordPress › Installation",
		"xpath": "//input[@id='language-continue']",
		"title": "WordPress › Installation",
		"url": "http://localhost:8080/wp-admin/install.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "id",
							"value": "language-continue"
						},
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "class",
							"value": "button button-primary button-large"
						},
						{
							"name": "value",
							"value": "Continue"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "step"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "id",
							"value": "setup"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "action",
							"value": "?step=1"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "wp-core-ui language-chooser"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "xml:lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 4
				}
			]
		},
		"screenshot": "Reports\\7bc3bd96-a517-a496-ff8f-4e8988520712.png"
	},
	"Log_In": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Log In",
		"object_library": "Chrome HTML",
		"window_name": "WordPress › Installation",
		"xpath": "//p[3]/a",
		"title": "WordPress › Installation",
		"url": "http://localhost:8080/wp-admin/install.php?step=2",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "class",
							"value": "button button-large"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Log In"
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "step"
						}
					],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "wp-core-ui"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "xml:lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				}
			]
		},
		"screenshot": "Reports\\bb644684-891c-e8ce-59eb-39dccb68491e.png"
	}
}