var saved_script_objects = {
	"Plugins_1": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "Plugins 1",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='menu-plugins']/a/div[3]",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wp-menu-name"
						}
					],
					"indexInParent": 2,
					"level": 0
				},
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "plugins.php"
						},
						{
							"name": "class",
							"value": "wp-has-submenu wp-not-current-submenu menu-top menu-icon-plugins"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						}
					],
					"indexInParent": 0,
					"level": 1,
					"text": "\n\nPlugins \n1"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "class",
							"value": "wp-has-submenu wp-not-current-submenu menu-top menu-icon-plugins"
						},
						{
							"name": "id",
							"value": "menu-plugins"
						}
					],
					"indexInParent": 8,
					"level": 2
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenu"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenuwrap"
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenumain"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Main menu"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Reports\\9b88caa7-bad6-084e-7c00-75a2bbe2df3e.png"
	},
	"Hello_Dolly": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "Hello Dolly",
		"object_library": "Chrome HTML",
		"window_name": "Plugins ‹ TTE Site Title — WordPress",
		"xpath": "//tr[3]/td[1]/strong",
		"title": "Plugins ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/plugins.php",
		"fullpath": {
			"path": [
				{
					"tagName": "strong",
					"attributes": [],
					"indexInParent": 0,
					"level": 0,
					"text": "Hello Dolly"
				},
				{
					"tagName": "td",
					"attributes": [
						{
							"name": "class",
							"value": "plugin-title column-primary"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "tr",
					"attributes": [
						{
							"name": "class",
							"value": "inactive"
						},
						{
							"name": "data-slug",
							"value": "hello-dolly"
						},
						{
							"name": "data-plugin",
							"value": "hello.php"
						}
					],
					"indexInParent": 2,
					"level": 2
				},
				{
					"tagName": "tbody",
					"attributes": [
						{
							"name": "id",
							"value": "the-list"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "table",
					"attributes": [
						{
							"name": "class",
							"value": "wp-list-table widefat plugins"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "bulk-action-form"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 12
				}
			]
		},
		"screenshot": "Reports\\6e8f9616-e40e-72dc-0e1f-901f6c4799e9.png"
	},
	"activated": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "activated",
		"object_library": "Chrome HTML",
		"window_name": "Plugins ‹ TTE Site Title — WordPress",
		"xpath": "//div[@id='message']/p/strong",
		"title": "Plugins ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/plugins.php?plugin_status=all&paged=1&s",
		"fullpath": {
			"path": [
				{
					"tagName": "strong",
					"attributes": [],
					"indexInParent": 0,
					"level": 0,
					"text": "activated"
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "message"
						},
						{
							"name": "class",
							"value": "updated notice is-dismissible"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Reports\\dec60eb6-aa3c-f7ee-04f1-6a1878145cd3.png"
	}
}